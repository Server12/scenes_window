# Changelog

### [1.0.5] - 2024-11-29
- fixes for Unity 6

### [1.0.4] - 2024-01-28
- fix changes from BuildSettings enabled/disabled for scenes
- add to AddCurrentToBuild - enable scene always

### [1.0.3] - 2023-07-27
- remove IMGUI Scenes window
- fix BuildSettings scenes change
- disable in playmode

### [1.0.2] - 2023-04-19
- fixes for Standalone builds

### [1.0.1] - 2023-03-26
- add new UIToolkit ScenesWindow
- refactor old ScenesWindow

### [1.0.0] - 2021-02-10

- Add Custom ScenesWindowEditor
