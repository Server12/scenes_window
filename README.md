Scenes Window helper 

Showing all project scenes in editor window, 
can open,add to favorite, live search.

![img.png](Content/window_screen.png)

License
MIT License

Copyright © 2021 Gameready
